﻿using System;
using System.Linq;

namespace RSPK.CommonCore.Factories
{
	public interface IFactoryProductMarker
	{
		string ProductKey { get; }
	}
}
