﻿using System;
using System.Linq;
using System.Reflection;

namespace RSPK.CommonCore.Factories
{
	public interface IFactoryBuilder<out TProduct>
		where TProduct: class
	{
		IFactory<TProduct> Factory { get; }
		void RegisterProducts(Assembly assembly);
	}
}
