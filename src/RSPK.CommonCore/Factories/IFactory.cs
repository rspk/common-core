using System;
using System.Collections.Generic;
using System.Linq;

namespace RSPK.CommonCore.Factories
{
	public interface IFactory<out TProduct>
		where TProduct: class
	{
		IList<string> KnownProducts { get; }
		bool IsKnownProduct(string productKey);
		TProduct Create(string productKey);
	}
}
