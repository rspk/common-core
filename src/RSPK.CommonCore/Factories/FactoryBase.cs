using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Castle.Core.Internal;
using System.Linq;
using System.Reflection;
using Castle.MicroKernel;
using Castle.MicroKernel.Registration;

namespace RSPK.CommonCore.Factories
{
	public abstract class FactoryBase<TProduct, TMarkerAttr>: IFactory<TProduct>, IFactoryBuilder<TProduct>
		where TProduct: class
		where TMarkerAttr: Attribute, IFactoryProductMarker
	{
		private readonly IList<string> _knownProducts = new List<string>();

		protected FactoryBase(IKernel container)
		{
			Container = container;
		}

		protected IKernel Container { get; }

		IFactory<TProduct> IFactoryBuilder<TProduct>.Factory => this;

		public IList<string> KnownProducts => new ReadOnlyCollection<string>(_knownProducts);

		public bool IsKnownProduct(string productKey)
		{
			return Container.HasComponent(typeof(TProduct)) && Container.HasComponent(productKey);
		}

		public virtual TProduct Create(string productKey)
		{
			return Container.Resolve<TProduct>(productKey);
		}

		public void RegisterProducts(Assembly assembly)
		{
			List<Tuple<string, Type>> types = assembly.GetTypes()
				.Where(type => typeof(TProduct).IsAssignableFrom(type))
				.SelectMany(type => type.GetAttributes<TMarkerAttr>()
					.Select(attr => attr.ProductKey)
					.Distinct(StringComparer.OrdinalIgnoreCase)
					.Select(productKey => Tuple.Create(productKey, type)))
				.ToList();
			foreach (Tuple<string, Type> tuple in types)
			{
				_knownProducts.Add(tuple.Item1);
				Container.Register(GetComponentRegistration(tuple.Item1, tuple.Item2));
			}
		}

		protected virtual ComponentRegistration<TProduct> GetComponentRegistration(string productKey, Type productType)
		{
			return Component.For<TProduct>().ImplementedBy(productType).Named(productKey);
		}
	}
}
