using System;
using System.Linq;
using Castle.Core.Logging;

namespace RSPK.CommonCore.Logging
{
	public static class LoggerExtensions
	{
		public static ILogger SetLogicalProperty(this ILogger logger, string name, object value)
		{
			if (logger is IExtendedLogger extendedLogger)
				extendedLogger.ThreadProperties[name] = value;
			return logger;
		}
	}
}
