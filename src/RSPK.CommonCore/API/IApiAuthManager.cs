﻿using System;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RSPK.CommonCore.API
{
	public interface IApiAuthManager
	{
		Task AuthenticateRequest(ApiRequestContext context, IApiClient client, HttpRequestHeaders requestHeaders);
	}
}
