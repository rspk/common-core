using System;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RSPK.CommonCore.API
{
	public class AuthorizationHeaderApiAuthManager: ApiAuthManagerBase
	{
		public AuthorizationHeaderApiAuthManager(string scheme)
		{
			Scheme = scheme;
		}

		public string Scheme { get; }

		public override async Task AuthenticateRequest(ApiRequestContext context, IApiClient client, HttpRequestHeaders requestHeaders)
		{
			AuthTokenItem authTokenItem = await GetAuthToken(context.ApiLogin, () => client.GetAuthToken(context));
			requestHeaders.Authorization = new AuthenticationHeaderValue(Scheme, authTokenItem.AuthToken);
		}
	}
}
