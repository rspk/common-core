using System;
using System.Linq;

namespace RSPK.CommonCore.API
{
	public enum ApiRequestBodyType
	{
		None,
		Json,
		FormData,
		PhpHttpQuery,
		JsonQueryString,
	}
}
