using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using JetBrains.Annotations;
using RSPK.CommonCore.Converters;
using RSPK.CommonCore.Extensions;

namespace RSPK.CommonCore.API
{
	public class ApiRequestContext
	{
		public ApiRequestContext()
		{
			Cookies = new CookieContainer();
			Properties = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
		}

		public ApiRequestContext([NotNull] ApiRequestContext baseContext)
		{
			if (baseContext == null) throw new ArgumentNullException(nameof(baseContext));
			Properties = new Dictionary<string, object>(baseContext.Properties, StringComparer.OrdinalIgnoreCase);
			Cookies = baseContext.Cookies;
		}

		public ApiRequestContext(ApiRequestContext baseContext, string apiLogin, string apiPassword)
			: this(baseContext)
		{
			ApiLogin = apiLogin;
			ApiPassword = apiPassword;
		}

		public ApiRequestContext(ApiRequestContext baseContext, string apiSubdomain, string apiLogin, string apiPassword)
			: this(baseContext)
		{
			ApiSubdomain = apiSubdomain;
			ApiLogin = apiLogin;
			ApiPassword = apiPassword;
		}

		public ApiRequestContext(ApiRequestContext baseContext, string apiSubdomain)
			: this(baseContext)
		{
			ApiSubdomain = apiSubdomain;
		}

		public CookieContainer Cookies { get; set; }

		public Dictionary<string, object> Properties { get; }

		public string ApiSubdomain
		{
			get => Properties.TryGetValue(nameof(ApiSubdomain)).TryConvertTo<string>().AsDefaultIfNotSuccess();
			set => Properties[nameof(ApiSubdomain)] = value;
		}

		public string ApiLogin
		{
			get => Properties.TryGetValue(nameof(ApiLogin)).TryConvertTo<string>().AsDefaultIfNotSuccess();
			set => Properties[nameof(ApiLogin)] = value;
		}

		public string ApiPassword
		{
			get => Properties.TryGetValue(nameof(ApiPassword)).TryConvertTo<string>().AsDefaultIfNotSuccess();
			set => Properties[nameof(ApiPassword)] = value;
		}

		public CancellationToken CancellationToken
		{
			get => Properties.TryGetValue(nameof(CancellationToken)).TryConvertTo<CancellationToken>().AsDefaultIfNotSuccess(CancellationToken.None);
			set => Properties[nameof(CancellationToken)] = value;
		}

		public ApiRequestBodyType RequestBodyType
		{
			get => Properties.TryGetValue(nameof(RequestBodyType)).TryConvertTo<ApiRequestBodyType>().AsDefaultIfNotSuccess();
			set => Properties[nameof(RequestBodyType)] = value;
		}

		public bool ThrowIfError
		{
			get => Properties.TryGetValue(nameof(ThrowIfError)).TryConvertTo<bool>().AsDefaultIfNotSuccess(true);
			set => Properties[nameof(ThrowIfError)] = value;
		}

		public bool RequiresAuthentication
		{
			get => Properties.TryGetValue(nameof(RequiresAuthentication)).TryConvertTo<bool>().AsDefaultIfNotSuccess(true);
			set => Properties[nameof(RequiresAuthentication)] = value;
		}

		public TimeSpan RequestTimeout
		{
			get => Properties.TryGetValue(nameof(RequestTimeout)).TryConvertTo<TimeSpan>().AsDefaultIfNotSuccess(ApiClientBase.DefaultRequestTimeout);
			set => Properties[nameof(RequestTimeout)] = value;
		}

		public bool WrapResponse
		{
			get => Properties.TryGetValue(nameof(WrapResponse)).TryConvertTo<bool>().AsDefaultIfNotSuccess();
			set => Properties[nameof(WrapResponse)] = value;
		}

		public AuthTokenItem AuthTokenItem
		{
			get => Properties.TryGetValue(nameof(AuthTokenItem)).TryConvertTo<AuthTokenItem>().AsDefaultIfNotSuccess();
			set => Properties[nameof(AuthTokenItem)] = value;
		}

		public bool IsAuthenticated => (AuthTokenItem?.ExpiresAt ?? DateTime.MinValue) >= DateTime.UtcNow;
	}
}
