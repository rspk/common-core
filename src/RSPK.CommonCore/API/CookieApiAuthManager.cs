﻿using System;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RSPK.CommonCore.API
{
	public class CookieApiAuthManager: ApiAuthManagerBase
	{
		public CookieApiAuthManager(bool controlledByRemoteServer)
		{
			ControlledByRemoteServer = controlledByRemoteServer;
		}

		public bool ControlledByRemoteServer { get; }

		public override async Task AuthenticateRequest(ApiRequestContext context, IApiClient client, HttpRequestHeaders requestHeaders)
		{
			if (ControlledByRemoteServer)
			{
				await GetAuthToken(context.ApiLogin, () => client.GetAuthToken(context));
				return;
			}
			throw new NotImplementedException();
		}
	}
}
