using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RSPK.CommonCore.API
{
	public interface IApiClient
	{
		ApiRequestContext CreateRequestContext(CancellationToken cancellationToken = default(CancellationToken));

		ApiRequestContext CreateRequestContext(string apiSubdomain, CancellationToken cancellationToken = default(CancellationToken));

		ApiRequestContext CreateRequestContext(string apiLogin, string apiPassword,
			CancellationToken cancellationToken = default(CancellationToken));

		ApiRequestContext CreateRequestContext(string apiSubdomain, string apiLogin, string apiPassword,
			CancellationToken cancellationToken = default(CancellationToken));

		Task<AuthTokenItem> GetAuthToken(ApiRequestContext context);
	}
}
