using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSPK.CommonCore.API
{
	internal class CustomEncodingProvider: EncodingProvider
	{
		private static readonly Dictionary<string, Encoding> __encodings = new Dictionary<string, Encoding>(StringComparer.OrdinalIgnoreCase)
			{
				{ "utf-8", Encoding.UTF8 },
				{ "utf-7", Encoding.UTF7 },
				{ "utf-32", Encoding.UTF32 },
			};

		public override Encoding GetEncoding(int codepage)
		{
			return CodePagesEncodingProvider.Instance.GetEncoding(codepage);
		}

		public override Encoding GetEncoding(string name)
		{
			if (!__encodings.TryGetValue(name.Trim('"'), out Encoding result))
				throw new NotImplementedException($"Can't find encoding for name '{name}'.");
			return result;
		}
	}
}
