﻿using System;
using System.IO;
using System.Linq;

namespace RSPK.CommonCore.Extensions
{
	public static class PathUtility
	{
		public static string Resolve(string absoluteOrRelativePath, Func<string> getBaseDir)
		{
			if (Path.IsPathRooted(absoluteOrRelativePath))
				return absoluteOrRelativePath;
			string baseDir = getBaseDir();
			return Path.GetFullPath(Path.Combine(baseDir, absoluteOrRelativePath));
		}

		public static string Resolve(string absoluteOrRelativePath, string baseDir)
		{
			return Resolve(absoluteOrRelativePath, () => baseDir);
		}
	}
}
