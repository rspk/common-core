using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace RSPK.CommonCore.Extensions
{
	public static class TypeExtensions
	{
		private interface IDefaultValueProvider
		{
			Type Type { get; }
			object DefaultValue { get; }
		}

		private class DefaultValueProvider<T>: IDefaultValueProvider
		{
			public Type Type => typeof(T);
			public object DefaultValue => default(T);
		}

		private static readonly List<IDefaultValueProvider> __defaultValueProviders = new List<IDefaultValueProvider>
			{
				new DefaultValueProvider<bool>(),
				new DefaultValueProvider<long>(),
				new DefaultValueProvider<ulong>(),
				new DefaultValueProvider<int>(),
				new DefaultValueProvider<uint>(),
				new DefaultValueProvider<short>(),
				new DefaultValueProvider<ushort>(),
				new DefaultValueProvider<sbyte>(),
				new DefaultValueProvider<byte>(),
				new DefaultValueProvider<char>(),
				new DefaultValueProvider<Guid>(),
			};

		private static readonly ConcurrentDictionary<Type, IDefaultValueProvider> __defaultValueProviderMap =
			new ConcurrentDictionary<Type, IDefaultValueProvider>(__defaultValueProviders.ToDictionary(p => p.Type));

		public static object GetDefaultValue(this Type type)
		{
			if (type.GetTypeInfo().IsClass)
				return null;
			IDefaultValueProvider provider = __defaultValueProviderMap.GetOrAdd(type,
				t => (IDefaultValueProvider) Activator.CreateInstance(typeof(DefaultValueProvider<>).MakeGenericType(t)));
			return provider.DefaultValue;
		}

		public static bool IsDefaultValue(this object value)
		{
			if (value == null)
				return true;
			Type type = value.GetType();
			return Equals(value, type.GetDefaultValue());
		}

		public static T IfDefault<T>(this T value, Func<T> mapDefaultValue)
		{
			return Equals(value, default(T)) ? mapDefaultValue() : value;
		}
	}
}
