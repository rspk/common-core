using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Web;
using JetBrains.Annotations;
using RSPK.CommonCore.Converters;

namespace RSPK.CommonCore.Extensions
{
	public static class UrlHelpers
	{
		public static string ToQueryString(this Dictionary<string, string> map)
		{
			return map.Count == 0
					? string.Empty
					: String.Join("&", map.Select(pair => WebUtility.UrlEncode(pair.Key) + '=' + WebUtility.UrlEncode(pair.Value)))
				;
		}

		public static string UriChangeScheme(this string uri, string schema)
		{
			int idx = uri.IndexOf("://", StringComparison.Ordinal);
			if (idx < 0)
				throw new InvalidOperationException();
			return schema + uri.Substring(idx);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Uri UriChangeScheme(this Uri uri, string schema)
		{
			return new Uri(UriChangeScheme(uri.ToString(), schema), UriKind.Absolute);
		}

		public static string UriChangeScheme(this string uri, Dictionary<string, string> schemaChangeMap)
		{
			int idx = uri.IndexOf("://", StringComparison.Ordinal);
			if (idx < 0)
				throw new InvalidOperationException();
			string oldScheme = uri.Substring(0, idx);
			string newScheme;
			if (schemaChangeMap.TryGetValue(oldScheme, out newScheme))
				return newScheme + uri.Substring(idx);
			return uri;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Uri UriChangeScheme(this Uri uri, Dictionary<string, string> schemaChangeMap)
		{
			return new Uri(UriChangeScheme(uri.ToString(), schemaChangeMap), UriKind.Absolute);
		}

		public static string SetQueryString<TRequest>(this string url, TRequest request)
		{
			return SetQueryString(new Uri(url), request).ToString();
		}

		public static Uri SetQueryString<TRequest>(this Uri url, TRequest request)
		{
			return SetQueryString(url, request.AsNameToObjectMap());
		}

		public static Uri SetQueryString(this Uri url, Dictionary<string, object> queryString)
		{
			string[] urlItems = url.PathAndQuery.Split(new[] { '?' }, 2, StringSplitOptions.RemoveEmptyEntries);
			string path;
			Dictionary<string, string> query;
			switch (urlItems.Length)
			{
				case 1:
					path = urlItems[0];
					query = new Dictionary<string, string>();
					break;
				case 2:
					path = urlItems[0];
					query = TryParseQueryString(urlItems[1], queryString.Comparer);
					break;
				default:
					throw new InvalidOperationException();
			}
			foreach (KeyValuePair<string, object> pair in queryString)
				query[pair.Key] = pair.Value.TryConvertTo<string>().AsDefaultIfNotSuccess();
			string newQueryString = query.ToQueryString().WithPrefixIfNotNullOrEmpty("?");
			string result = $"{url.Scheme}://{url.Authority}{path}{newQueryString}";
			return new Uri(result);
		}

		public static Uri SetQueryString(this Uri url, string paramName, object paramValue)
		{
			var queryString = new Dictionary<string, object>
				{
					{ paramName, paramValue },
				};
			return SetQueryString(url, queryString);
		}

		public static Dictionary<string, string> TryParseQueryString(this string queryString, IEqualityComparer<string> comparer = null)
		{
			if (string.IsNullOrEmpty(queryString))
				return new Dictionary<string, string>();
			return queryString.Split('&')
				.Select(ParseQueryParameter)
				.ToDictionary(tuple => tuple.name, tuple => tuple.value, comparer ?? EqualityComparer<string>.Default);
		}

		private static (string name, string value) ParseQueryParameter([NotNull] string queryParameter)
		{
			if (queryParameter == null) throw new ArgumentNullException(nameof(queryParameter));
			string[] items = queryParameter.Split(new[] { '=' }, 2);
			switch (items.Length)
			{
				case 2:
					return (name: items[0], value: HttpUtility.UrlDecode(items[1]));
				case 1:
					return (name: items[0], value: null);
				default:
					throw new InvalidOperationException();
			}
		}
	}
}
