using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RSPK.CommonCore.Extensions
{
	public static class EmbeddedResourceExtensions
	{
		public static async Task<T> ReadEmbeddedResource<T>(this Assembly assembly, string @namespace, string name, Func<StreamReader, Task<T>> read)
		{
			string resourceName = $"{@namespace}.{name}";
			using (Stream stream = assembly.GetManifestResourceStream(resourceName))
			{
				if (stream == null)
					throw new InvalidOperationException($"Can't find embedded resource with name '{resourceName}'.");
				using (var reader = new StreamReader(stream))
					return await read(reader);
			}
		}

		public static Task<string> ReadEmbeddedResourceAsStringAsync(this Assembly assembly, string @namespace, string name)
		{
			return ReadEmbeddedResource(assembly, @namespace, name, reader => reader.ReadToEndAsync());
		}

		public static Task<string> ReadEmbeddedResourceAsStringAsync(this Assembly assembly, Type namespaceType, string name)
		{
			return ReadEmbeddedResourceAsStringAsync(assembly, namespaceType.Namespace, name);
		}

		public static async Task<TObject> ReadEmbeddedResourceAsJsonObjectAsync<TObject>(this Assembly assembly, string @namespace, string name)
		{
			string json = await ReadEmbeddedResourceAsStringAsync(assembly, @namespace, name);
			return JsonConvert.DeserializeObject<TObject>(json);
		}

		public static Task<TObject> ReadEmbeddedResourceAsJsonObjectAsync<TObject>(this Assembly assembly, Type namespaceType, string name)
		{
			return ReadEmbeddedResourceAsJsonObjectAsync<TObject>(assembly, namespaceType.Namespace, name);
		}
	}
}
