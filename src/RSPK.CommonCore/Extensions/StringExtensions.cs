using System;
using System.Linq;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;

namespace RSPK.CommonCore.Extensions
{
	public static class StringExtensions
	{
		public static string WithPrefixIfNotNullOrEmpty(this string value, [NotNull] string prefix)
		{
			if (String.IsNullOrEmpty(value))
				return value;
			return prefix + value;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static string AsEmptyIfNull(this string value)
		{
			return value ?? String.Empty;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static string AsNullIfEmpty(this string value)
		{
			return value == String.Empty ? null : value;
		}

		public static string ToStringOrNull(this object obj)
		{
			if (obj == null)
				return null;
			return Convert.ToString(obj);
		}

		public static (bool isTrue, string value, Func<string, T> then) IfNullThen<T>(this string value, Func<T> then)
		{
			return (isTrue: value == null, value: value, then: v => then());
		}

		public static (bool isTrue, string value, Func<string, T> then) IfNullThen<T>(this string value, Func<string, T> then)
		{
			return (isTrue: value == null, value: value, then: then);
		}

		public static (bool isTrue, string value, Func<string, T> then) IfNullThen<T>(this string value, T thenValue = default(T))
		{
			return (isTrue: value == null, value: value, then: v => thenValue);
		}

		public static (bool isTrue, string value, Func<string, T> then) IfNotNullThen<T>(this string value, Func<T> then)
		{
			return (isTrue: value != null, value: value, then: v => then());
		}

		public static (bool isTrue, string value, Func<string, T> then) IfNotNullThen<T>(this string value, Func<string, T> then)
		{
			return (isTrue: value != null, value: value, then: then);
		}

		public static (bool isTrue, string value, Func<string, T> then) IfNotNullThen<T>(this string value, T thenValue = default(T))
		{
			return (isTrue: value != null, value: value, then: v => thenValue);
		}

		public static T Else<T>(this (bool isTrue, string value, Func<string, T> then) condition, Func<T> @else)
		{
			return condition.isTrue ? condition.then(condition.value) : @else();
		}

		public static T Else<T>(this (bool isTrue, string value, Func<string, T> then) condition, Func<string, T> @else)
		{
			return condition.isTrue ? condition.then(condition.value) : @else(condition.value);
		}

		public static T Else<T>(this (bool isTrue, string value, Func<string, T> then) condition, T elseValue)
		{
			return condition.isTrue ? condition.then(condition.value) : elseValue;
		}

		public static T ElseDefault<T>(this (bool isTrue, string value, Func<string, T> then) condition)
		{
			return condition.isTrue ? condition.then(condition.value) : default(T);
		}

		public static string TruncStart(this string value, int maxLength, string truncatedValueMarker = "...")
		{
			if (value == null)
				return null;
			if (maxLength <= 0)
				return string.Empty;
			if (value.Length <= maxLength)
				return value;
			if (truncatedValueMarker.Length >= maxLength)
				return truncatedValueMarker.Substring(truncatedValueMarker.Length - maxLength, maxLength);
			return truncatedValueMarker + value.Substring(value.Length - maxLength + truncatedValueMarker.Length, maxLength - truncatedValueMarker.Length);
		}

		public static string TruncEnd(this string value, int maxLength, string truncatedValueMarker = "...")
		{
			if (value == null)
				return null;
			if (maxLength <= 0)
				return string.Empty;
			if (value.Length <= maxLength)
				return value;
			if (truncatedValueMarker.Length >= maxLength)
				return truncatedValueMarker.Substring(0, Math.Min(maxLength, value.Length));
			return value.Substring(0, maxLength - truncatedValueMarker.Length) + truncatedValueMarker;
		}
	}
}
