﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;

namespace RSPK.CommonCore.Extensions
{
	public static class ObjectExtensions
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static T With<T>(this T obj, Action<T> action)
		{
			action(obj);
			return obj;
		}
	}
}
