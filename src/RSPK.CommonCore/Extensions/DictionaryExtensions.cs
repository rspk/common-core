using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace RSPK.CommonCore.Extensions
{
	public static class DictionaryExtensions
	{
		public static TValue TryGetValue<TKey, TValue>(this IDictionary<TKey, TValue> collection, TKey key, Func<TKey, TValue> getDefaultValue)
		{
			TValue result;
			if (!collection.TryGetValue(key, out result))
				result = (getDefaultValue ?? (k => default(TValue)))(key);
			return result;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static TValue TryGetValue<TKey, TValue>(this IDictionary<TKey, TValue> collection, TKey key, Func<TValue> getDefaultValue)
		{
			return collection.TryGetValue(key, dummy => (getDefaultValue ?? (() => default(TValue)))());
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static TValue TryGetValue<TKey, TValue>(this IDictionary<TKey, TValue> collection, TKey key, TValue defaultValue = default(TValue))
		{
			return collection.TryGetValue(key, dummy => defaultValue);
		}

		public static TResult TryGetValue<TKey, TValue, TResult>(this IDictionary<TKey, TValue> collection, TKey key, Func<TValue, TResult> extractResult,
			Func<TKey, TResult> getDefaultValue)
		{
			if (collection.TryGetValue(key, out TValue value))
				return extractResult(value);
			return getDefaultValue(key);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static TResult TryGetValue<TKey, TValue, TResult>(this IDictionary<TKey, TValue> collection, TKey key, Func<TValue, TResult> extractResult,
			Func<TResult> getDefaultValue)
		{
			return collection.TryGetValue(key, extractResult, dummy => (getDefaultValue ?? (() => default(TResult)))());
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static TResult TryGetValue<TKey, TValue, TResult>(this IDictionary<TKey, TValue> collection, TKey key, Func<TValue, TResult> extractResult,
			TResult defaultValue = default(TResult))
		{
			return collection.TryGetValue(key, extractResult, dummy => defaultValue);
		}
	}
}
