﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RSPK.CommonCore.Extensions
{
	public static class EnumerableExtensions
	{
		public static IEnumerable<TResult> FullOuterJoin<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer,
			IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector,
			Func<TKey, TOuter, TInner, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			ILookup<TKey, TInner> innerLookup = inner.ToLookup(innerKeySelector, comparer);

			var joined = new List<TKey>();
			foreach (TOuter outerItem in outer)
			{
				TKey key = outerKeySelector(outerItem);
				if (innerLookup.Contains(key))
				{
					joined.Add(key);
					foreach (TInner innerItem in innerLookup[key])
						yield return resultSelector(key, outerItem, innerItem);
				}
				else
					yield return resultSelector(key, outerItem, default(TInner));
			}

			foreach (IGrouping<TKey, TInner> grouping in innerLookup)
			{
				if (!joined.Contains(grouping.Key, comparer))
				{
					foreach (TInner innerItem in grouping)
						yield return resultSelector(grouping.Key, default(TOuter), innerItem);
				}
			}
		}

		public static IEnumerable<TResult> FullOuterJoin<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer,
			IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector,
			Func<TKey, TOuter, TInner, TResult> resultSelector)
		{
			return FullOuterJoin(outer, inner, outerKeySelector, innerKeySelector, resultSelector, EqualityComparer<TKey>.Default);
		}

		public static int IndexOf<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			using (IEnumerator<TSource> enumerator = source.GetEnumerator())
			{
				int result = 0;
				while (enumerator.MoveNext())
				{
					if (predicate(enumerator.Current))
						return result;
					result++;
				}
				return -1;
			}
		}

		public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
		{
			foreach (T item in source)
				action(item);
		}

		public static IEnumerable<IList<T>> ChunkBy<T>(this IEnumerable<T> source, int chunkSize)
		{
			if (chunkSize <= 0)
				throw new ArgumentOutOfRangeException(nameof(chunkSize), "Chunk size should be greater then zero.");

			using (IEnumerator<T> enumerator = source.GetEnumerator())
			{
				if (enumerator.MoveNext())
					while (true)
					{
						IList<T> result = new List<T>();
						for (int i = 0; i < chunkSize; i++)
						{
							result.Add(enumerator.Current);
							if (!enumerator.MoveNext())
							{
								if (result.Any())
									yield return result;
								yield break;
							}
						}
						yield return result;
					}
			}
		}
	}
}
