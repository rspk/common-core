using System;
using System.Linq;

namespace RSPK.CommonCore.DependencyInjection
{
	[AttributeUsage(AttributeTargets.Property)]
	public class PropertyInjectAttribute: Attribute
	{
		public PropertyInjectAttribute()
		{
			IsOptional = true;
		}

		public string DependencyKey { get; set; }
		public bool IsOptional { get; set; }
	}
}
