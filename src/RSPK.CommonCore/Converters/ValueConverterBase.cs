using System;
using System.Linq;

namespace RSPK.CommonCore.Converters
{
	public abstract class ValueConverterBase<TValue>: IValueConverter<TValue>
	{
		public abstract ConvertAttempt TryConvertTo(TValue value, Type toType);

		public ConvertAttempt<TResult> TryConvertTo<TResult>(TValue value)
		{
			return TryConvertTo(value, typeof(TResult)).Cast<TResult>();
		}
	}
}
