using System;
using System.Linq;

namespace RSPK.CommonCore.Converters.ValueTypeConverters
{
	public class StringToBooleanConverter: ValueTypeConverterBase<string, bool>
	{
		public override ConvertAttempt<bool> TryConvert(string sourceValue)
		{
			bool result;
			if (bool.TryParse(sourceValue, out result))
				return ConvertAttempt.Success(result);
			ConvertAttempt<int> attempt = new ValueTypeParser<int>(int.TryParse).TryConvert(sourceValue);
			if (attempt.IsSuccess)
				return ConvertAttempt.Success(attempt.Result != 0);
			return ConvertAttempt.Fail<bool>();
		}
	}
}
