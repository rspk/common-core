﻿using System;
using System.Linq;

namespace RSPK.CommonCore.Converters
{
	public delegate bool TryParseDelegate<T>(string value, out T result);
}
