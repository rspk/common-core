using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using Newtonsoft.Json.Linq;
using RSPK.CommonCore.Converters.CollectionConverters;
using RSPK.CommonCore.Converters.ValueTypeConverters;

namespace RSPK.CommonCore.Converters
{
	public static class ConvertExtensions
	{
		private static readonly ConcurrentDictionary<Type, IObjectConverter> __objectConverters =
			new ConcurrentDictionary<Type, IObjectConverter>(new Dictionary<Type, IObjectConverter>
				{
					{ typeof(string), new ObjectToStringConverter() },
					{ typeof(byte[]), new ObjectToByteArrayConverter() },
				});

		public static ConvertAttempt TryConvertTo(this object source, Type toType)
		{
			return ConvertInvoker.GetOrCreateInvoker(toType).TryConvertTo(source);
		}

		public static ConvertAttempt<T> TryConvertTo<T>(this object source)
		{
			Type toType = typeof(T);
			if (toType.GetTypeInfo().IsValueType)
				return TryConvertToValueType<T>(source);
			return TryConvertToObject<T>(source);
		}

		private static ConvertAttempt<T> TryConvertToObject<T>(object source)
		{
			switch (source)
			{
				case null:
					return ConvertAttempt.Success(default(T));
				case T targetTypeValue:
					return ConvertAttempt.Success(targetTypeValue);
				case JValue _:
					break;
				case string _:
					break;
				case IEnumerable enumerable:
					return TryConvertEnumerableToObject<T>(enumerable);
			}
			IObjectConverter converter;
			if (!__objectConverters.TryGetValue(typeof(T), out converter))
				return ConvertAttempt.Fail<T>();
			return converter.TryConvert(source).Cast<T>();
		}

		private static readonly Dictionary<Type, Func<Type, IObjectConverter>> __genericCollectionConverters = new Dictionary<Type, Func<Type, IObjectConverter>>
			{
				{ typeof(List<>), EnumerableToListConverter.GetOrCreateConverter },
				{ typeof(IList<>), EnumerableToListConverter.GetOrCreateConverter },
			};

		private static ConvertAttempt<T> TryConvertEnumerableToObject<T>(IEnumerable source)
		{
			Type toType = typeof(T);
			if (toType.IsArray)
			{
				Type elementType = toType.GetElementType();
				IObjectConverter converter = EnumerableToArrayConverter.GetOrCreateConverter(elementType);
				return converter.TryConvert(source).Cast<T>();
			}
			if (toType.IsGenericType)
			{
				Func<Type, IObjectConverter> create;
				if (__genericCollectionConverters.TryGetValue(toType.GetGenericTypeDefinition(), out create))
				{
					Type elementType = toType.GetGenericArguments()[0];
					IObjectConverter converter = create(elementType);
					return converter.TryConvert(source).Cast<T>();
				}
			}
			throw new NotImplementedException();
		}

		private static ConvertAttempt<T> TryConvertToValueType<T>(object source)
		{
			if (source == null)
				return ConvertAttempt.Fail<T>();
			Type fromType = source.GetType();
			if (typeof(T).IsAssignableFrom(fromType))
				return ConvertAttempt.Success((T) source);
			switch (source)
			{
				case string stringValue:
					return new StringToValueTypeConverter().TryConvertTo<T>(stringValue);
				case byte[] value:
					return new ByteArrayToValueConverter().TryConvertTo<T>(value);
				case ValueType value:
					return new ValueTypeToValueTypeConverter().TryConvertTo<T>(value);
				default:
					throw new InvalidOperationException();
			}
		}

		public static T ConvertTo<T>(this object source)
		{
			ConvertAttempt<T> attempt = source.TryConvertTo<T>();
			if (!attempt.IsSuccess)
				throw new InvalidOperationException($"Can't convert value '{source}' to type '{typeof(T)}'.");
			return attempt.Result;
		}

		public static object ConvertTo(this object source, Type toType)
		{
			ConvertAttempt attempt = source.TryConvertTo(toType);
			if (!attempt.IsSuccess)
				throw new InvalidOperationException($"Can't convert value '{source}' to type '{toType}'.");
			return attempt.Result;
		}
	}
}
