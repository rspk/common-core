using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using RSPK.CommonCore.Converters.ValueTypeConverters;

namespace RSPK.CommonCore.Converters
{
	public interface IValueConverter<in TValue>
	{
		ConvertAttempt TryConvertTo(TValue value, Type toType);
		ConvertAttempt<TResult> TryConvertTo<TResult>(TValue value);
	}

	public class StringToValueTypeConverter: ValueConverterBase<string>
	{
		private static readonly ConcurrentDictionary<Type, IValueTypeConverter> __converterMap =
			new ConcurrentDictionary<Type, IValueTypeConverter>(
				new Dictionary<Type, IValueTypeConverter>
					{
						{ typeof(bool), new StringToBooleanConverter() },
						{ typeof(long), new ValueTypeParser<long>(long.TryParse) },
						{ typeof(ulong), new ValueTypeParser<ulong>(ulong.TryParse) },
						{ typeof(int), new ValueTypeParser<int>(int.TryParse) },
						{ typeof(uint), new ValueTypeParser<uint>(uint.TryParse) },
						{ typeof(short), new ValueTypeParser<short>(short.TryParse) },
						{ typeof(ushort), new ValueTypeParser<ushort>(ushort.TryParse) },
						{ typeof(sbyte), new ValueTypeParser<sbyte>(sbyte.TryParse) },
						{ typeof(byte), new ValueTypeParser<byte>(byte.TryParse) },
						{ typeof(decimal), new ValueTypeParser<decimal>(decimal.TryParse) },
						{ typeof(float), new ValueTypeParser<float>(float.TryParse) },
						{ typeof(double), new ValueTypeParser<double>(double.TryParse) },
						{ typeof(Guid), new ValueTypeParser<Guid>(Guid.TryParse) },
						{ typeof(DateTime), new ValueTypeParser<DateTime>(DateTime.TryParse) },
						{ typeof(TimeSpan), new ValueTypeParser<TimeSpan>(TimeSpan.TryParse) },
					}
			);

		public override ConvertAttempt TryConvertTo(string value, Type toType)
		{
			IValueTypeConverter converter;
			if (!__converterMap.TryGetValue(toType, out converter))
			{
				TypeInfo typeInfo = toType.GetTypeInfo();
				if (typeInfo.IsGenericType && typeInfo.GetGenericTypeDefinition() == typeof(Nullable<>))
					converter = __converterMap.GetOrAdd(toType, NullableValueTypeConverter.Create);
				else if (typeInfo.IsEnum)
					converter = __converterMap.GetOrAdd(toType, EnumParser.Create);
				else
					return ConvertAttempt.Fail(toType);
			}

			return converter.TryConvert(value);
		}
	}
}
