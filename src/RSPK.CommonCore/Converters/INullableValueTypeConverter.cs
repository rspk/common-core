using System;
using System.Linq;

namespace RSPK.CommonCore.Converters
{
	public interface INullableValueTypeConverter<TTarget>: IValueTypeConverter
		where TTarget: struct
	{
		new ConvertAttempt<TTarget?> TryConvert(object sourceValue);
	}
}
