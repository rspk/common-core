using System;
using System.Linq;

namespace RSPK.CommonCore.Integrations
{
	public interface IConfigValueProvider
	{
		T GetValue<T>(string key, Func<T> getDefaultValue);
		T GetValue<T>(string key, T defaultValue = default(T));
	}
}
