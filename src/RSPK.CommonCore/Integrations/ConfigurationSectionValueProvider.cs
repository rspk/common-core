using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using RSPK.CommonCore.Converters;

namespace RSPK.CommonCore.Integrations
{
	public class ConfigurationSectionValueProvider: IConfigValueProvider
	{
		private readonly IConfigurationSection _config;

		public ConfigurationSectionValueProvider(IConfigurationSection config)
		{
			_config = config;
		}

		public T GetValue<T>(string key, Func<T> getDefaultValue)
		{
			getDefaultValue = getDefaultValue ?? (() => default(T));
			Type targetType = typeof(T);
			if (targetType == typeof(string))
			{
				string strValue;
				return TryGetValue(key, out strValue) ? (T) (object) strValue : getDefaultValue();
			}
			if (typeof(IEnumerable).IsAssignableFrom(targetType))
			{
				List<string> tempValues = GetConfigValueList(key);
				if (tempValues.Count == 0)
					return getDefaultValue();

				if (targetType.IsArray)
				{
					ConvertAttempt attempt = tempValues.TryConvertTo(targetType);
					return attempt.IsSuccess ? (T) attempt.Result : getDefaultValue();
				}
				if (targetType.IsGenericType)
				{
					Type genericType = targetType.GetGenericTypeDefinition();
					if (genericType == typeof(List<>) || genericType == typeof(IEnumerable<>) || genericType == typeof(ICollection<>) || genericType == typeof(IList<>))
					{
						ConvertAttempt attempt = tempValues.TryConvertTo(targetType);
						return attempt.IsSuccess ? (T) attempt.Result : getDefaultValue();
					}
				}
				if (targetType == typeof(IEnumerable) || targetType == typeof(ICollection) || targetType == typeof(IList))
				{
					ConvertAttempt attempt = tempValues.TryConvertTo(targetType);
					return attempt.IsSuccess ? (T) attempt.Result : getDefaultValue();
				}
				throw new NotSupportedException();
			}
			string value = _config[key];
			if (value != null)
			{
				ConvertAttempt<T> attempt = value.TryConvertTo<T>();
				if (attempt.IsSuccess)
					return attempt.Result;
			}
			return getDefaultValue();
		}

		public T GetValue<T>(string key, T defaultValue = default(T))
		{
			return GetValue(key, () => defaultValue);
		}

		private bool TryGetValue(string key, out string value)
		{
			value = _config[key];
			return value != null;
		}

		private List<string> GetConfigValueList(string key)
		{
			List<string> tempValues = new List<string>();
			for (int i = 0; i < int.MaxValue; i++)
			{
				string value = _config[$"{key}:{i}"];
				if (value == null)
					break;
				tempValues.Add(value);
			}
			return tempValues;
		}
	}
}
