using System;
using System.Collections.Generic;
using System.Linq;
using RSPK.CommonCore.Configuration;
using RSPK.CommonCore.Utils;

namespace RSPK.CommonCore.Integrations
{
	public interface IIntegrationsConfiguration
	{
		TimeSpan LoadServiceHistoryInterval { get; set; }
		TimeZoneIdSystemType ServerTimeZoneIdSystem { get; }
		IProxyConfiguration Proxy { get; }
		IDictionary<string, IApiAgentConfiguration> Agents { get; }
	}
}
