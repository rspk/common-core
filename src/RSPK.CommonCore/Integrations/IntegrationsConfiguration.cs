using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.Extensions.Configuration;
using RSPK.CommonCore.Configuration;
using RSPK.CommonCore.Converters;
using RSPK.CommonCore.Extensions;
using RSPK.CommonCore.Utils;

namespace RSPK.CommonCore.Integrations
{
	public class IntegrationsConfiguration: IIntegrationsConfiguration
	{
		protected static readonly TimeSpan __defaultLoadServiceHistoryInterval = TimeSpan.FromSeconds(60);

		private readonly IDictionary<string, IApiAgentConfiguration> _agents = new Dictionary<string, IApiAgentConfiguration>();

		public IntegrationsConfiguration(IConfigurationSection config)
		{
			if (config != null)
			{
				LoadServiceHistoryInterval = config[nameof(LoadServiceHistoryInterval)]
					.IfNotNullThen(value => value.TryConvertTo<TimeSpan>().AsDefaultIfNotSuccess(__defaultLoadServiceHistoryInterval))
					.Else(__defaultLoadServiceHistoryInterval);
				ServerTimeZoneIdSystem = config[nameof(ServerTimeZoneIdSystem)]
						.IfNotNullThen(value => value.TryConvertTo<TimeZoneIdSystemType>().AsDefaultIfNotSuccess(TimeZoneIdSystemType.Windows))
						.Else(TimeZoneIdSystemType.Windows)
					;
				Proxy = ProxyConfiguration.Create(config.GetSection(nameof(Proxy)));
				IConfigurationSection agentsConfig = config.GetSection(nameof(Agents));
				if (agentsConfig != null)
				{
					foreach (IConfigurationSection agentConfig in agentsConfig.GetChildren())
					{
						IApiAgentConfiguration agent = ApiAgentConfiguration.Create(agentConfig, Proxy);
						_agents.Add(agent.Key, agent);
					}
				}
			}
		}

		public TimeSpan LoadServiceHistoryInterval { get; set; }
		public TimeZoneIdSystemType ServerTimeZoneIdSystem { get; }
		public IProxyConfiguration Proxy { get; }

		public IDictionary<string, IApiAgentConfiguration> Agents => new ReadOnlyDictionary<string, IApiAgentConfiguration>(_agents);
	}
}
