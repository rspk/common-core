using System;
using System.Linq;
using System.Net;
using RSPK.CommonCore.Configuration;
using RSPK.CommonCore.Utils;

namespace RSPK.CommonCore.Integrations
{
	public interface IApiAgentConfiguration
	{
		string Key { get; }
		IProxyConfiguration Proxy { get; }
		string BaseUrl { get; }
		NetworkCredential Credentials { get; }
		TimeZoneIdSystemType TimeZoneIdSystem { get; }
		bool IgnoreSslErrors { get; }

		IConfigValueProvider Config { get; }
	}
}
