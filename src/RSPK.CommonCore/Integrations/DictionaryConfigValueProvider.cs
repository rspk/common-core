using System;
using System.Collections.Generic;
using System.Linq;
using RSPK.CommonCore.Converters;

namespace RSPK.CommonCore.Integrations
{
	public class DictionaryConfigValueProvider: IConfigValueProvider
	{
		private readonly IDictionary<string, object> _values;

		public DictionaryConfigValueProvider(IDictionary<string, object> values)
		{
			_values = values;
		}

		public T GetValue<T>(string key, Func<T> getDefaultValue)
		{
			object value;
			if (_values.TryGetValue(key, out value))
			{
				ConvertAttempt<T> attempt = value.TryConvertTo<T>();
				if (attempt.IsSuccess)
					return attempt.Result;
			}
			return getDefaultValue();
		}

		public T GetValue<T>(string key, T defaultValue = default(T))
		{
			return GetValue(key, () => defaultValue);
		}
	}
}
