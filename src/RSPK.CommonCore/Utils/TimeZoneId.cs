using System;
using System.Linq;

namespace RSPK.CommonCore.Utils
{
	public struct TimeZoneId
	{
		public TimeZoneId(TimeZoneIdSystemType idSystem, string value)
		{
			IdSystem = idSystem;
			Value = value;
		}

		public TimeZoneIdSystemType IdSystem { get; set; }
		public string Value { get; set; }

		public TimeZoneId ConvertTo(TimeZoneIdSystemType idSystem)
		{
			return new TimeZoneId(idSystem, TimeZoneHelpers.ConvertTimeZoneId(Value, IdSystem, idSystem));
		}
	}
}
