using System;
using System.Linq;
using System.Net;
using Microsoft.Extensions.Configuration;
using RSPK.CommonCore.Extensions;

namespace RSPK.CommonCore.Configuration
{
	public static class ConfigurationExtensions
	{
		public static NetworkCredential LoadCredentials(this IConfigurationSection credentialsConfig, NetworkCredential parentCredentials = null)
		{
			if (credentialsConfig == null)
				return parentCredentials /* ?? CredentialCache.DefaultNetworkCredentials*/;
			string userName = credentialsConfig[nameof(NetworkCredential.UserName)]
						.IfNullThen(parentCredentials?.UserName)
						.Else(value => value)
					?? String.Empty
				;
			string password = credentialsConfig[nameof(NetworkCredential.Password)]
						.IfNullThen(parentCredentials?.Password)
						.Else(value => value)
					?? String.Empty
				;
			return String.IsNullOrEmpty(userName)
					? null //CredentialCache.DefaultNetworkCredentials
					: new NetworkCredential(userName, password)
				;
		}
	}
}
