using System;
using System.Linq;
using System.Net;
using Microsoft.Extensions.Configuration;
using RSPK.CommonCore.Converters;
using RSPK.CommonCore.Extensions;

namespace RSPK.CommonCore.Configuration
{
	public class ProxyConfiguration: IProxyConfiguration
	{
		public static IProxyConfiguration NoProxy = new ProxyConfiguration(false, null, null);

		private ProxyConfiguration(bool useProxy, Uri proxyUrl, NetworkCredential credentials)
		{
			UseProxy = useProxy;
			ProxyUrl = proxyUrl;
			Credentials = credentials;
		}

		public bool UseProxy { get; set; }
		public Uri ProxyUrl { get; set; }
		public NetworkCredential Credentials { get; set; }

		public IWebProxy GetWebProxy()
		{
			return UseProxy ? new WebProxyData(this) : WebRequest.DefaultWebProxy;
		}

		public static IProxyConfiguration Create(IConfigurationSection section, IProxyConfiguration parentConfig = null)
		{
			if (section == null)
				return NoProxy;

			bool useProxy = section[nameof(UseProxy)]
				.IfNullThen(parentConfig?.UseProxy ?? false)
				.Else(value => value.TryConvertTo<bool>().AsDefaultIfNotSuccess());
			Uri proxyUrl = section[nameof(ProxyUrl)]
				.IfNullThen(parentConfig?.ProxyUrl)
				.Else(value => new Uri(value, UriKind.Absolute));
			NetworkCredential credentials = section.GetSection(nameof(Credentials)).LoadCredentials(parentConfig?.Credentials);

			return new ProxyConfiguration(useProxy, proxyUrl, credentials);
		}

		public static IProxyConfiguration Create(Uri proxyUrl, NetworkCredential credentials = null, bool useProxy = true)
		{
			return new ProxyConfiguration(useProxy, proxyUrl, credentials);
		}

		public static IProxyConfiguration Create(string proxyUrl, NetworkCredential credentials = null, bool useProxy = true)
		{
			return new ProxyConfiguration(useProxy, new Uri(proxyUrl, UriKind.Absolute), credentials);
		}
	}
}
