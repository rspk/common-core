using System;
using System.Linq;
using System.Net;

namespace RSPK.CommonCore.Configuration
{
	public interface IProxyConfiguration
	{
		bool UseProxy { get; }
		Uri ProxyUrl { get; }
		NetworkCredential Credentials { get; }

		IWebProxy GetWebProxy();
	}
}
