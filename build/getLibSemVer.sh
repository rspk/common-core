#!/bin/bash

function getMaxTagsVersion()
{
	local maxVersion=0;
	for tag in `git show-ref --tags | awk -F "[ /]" '/^([0-9a-f]{40}\s\<refs\/tags\>)\/([[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+)$/{print $4}'`; do
		local refVersion=$(($(echo "$tag" | awk -F "." '{print ($1)*1000000+($2)*1000+($3)}')));
		if [ $refVersion -gt $maxVersion ]; then
			maxVersion=$refVersion;
		fi
	done;
	echo $maxVersion;
}

function getMaxHeadsVersion()
{
	local maxVersion=0;
	for tag in `git show-ref --heads | awk -F "[ /]" '/^[0-9a-f]{40}\s\<refs\/heads\>\/(\<release\>|\<hotfix\>)\/([[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+)$/{print $5}'`; do
		local refVersion=$(($(echo "$tag" | awk -F "." '{print ($1)*1000000+($2)*1000+($3)}')));
		if [ $maxVersion -lt $refVersion ]; then
			maxVersion=$refVersion;
		fi
	done;
	echo $maxVersion;
}

function getMaxRemotesVersion()
{
	local maxVersion=0;
	for tag in `git show-ref | grep "refs/remotes/" | awk -F "[ /]" '/^[0-9a-f]{40}\s\<refs\/remotes\>\/[^/]+\/(\<release\>|\<hotfix\>)\/([[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+)$/{print $6}'`; do
		local refVersion=$(($(echo "$tag" | awk -F "." '{print ($1)*1000000+($2)*1000+($3)}')));
		if [ $maxVersion -lt $refVersion ]; then
			maxVersion=$refVersion;
		fi
	done;
	echo $maxVersion;
}

function getMaxVersion()
{
	local maxVersion=$(($(getMaxTagsVersion)));
	local headVersion=$(($(getMaxHeadsVersion)));
	local maxRemotesVersion=$(($(getMaxRemotesVersion)));
	if [ $maxVersion -lt $headVersion ]; then
		maxVersion=$headVersion;
	fi
	if [ $maxVersion -lt $maxRemotesVersion ]; then
		maxVersion=$maxRemotesVersion;
	fi
	echo $maxVersion;
}

branch=$1
isFinal=0;

if [ ! -n "$branch" ]; then
	branch=$(git branch | grep "*" | awk -F " " '{print $2}');
	[[ $branch =~ ^[^/]+\/[^/]+$ ]] && branch=$(echo $branch | awk -F "/" '{print $2}');
fi

version=0;
suffix=;

rcVersion=$(echo $branch | awk -F "[.]" '/^([[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+)$/{print ($1)*1000000+($2)*1000+($3)}');

branchPrefix=$(echo $branch | awk -F "/" '/^([^/]+)\/.+$/{print $1}');

if [ -n "$rcVersion" ]; then
	version=$rcVersion;
	suffix="-rc";
elif [ $branch == "develop" ]; then
	version=$(($(getMaxVersion)+1000));
	suffix="-beta";
elif [ $branch == "master" ]; then
	currentCommitHash=$(git show-ref --heads | grep "refs/heads/$branch" | awk -F " " '/^.+$/{print $1}');
	currentCommitTag=$(git show-ref --tags -d | grep "$currentCommitHash" | awk -F "[ ^/]" '/^([0-9a-f]{40}\s\<refs\/tags\>)\/([[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+)\^\{\}$/{print $4}');
	if [ ! -n "$currentCommitTag" ]; then
		echo "Simple commits is not supported by semantic versioning. Please create release or hotfix branch."
		exit 1;
	fi
	version=$(echo "$currentCommitTag" | awk -F "." '{print ($1)*1000000+($2)*1000+($3)}');
	isFinal=1;
else
	version=$(($(getMaxVersion)+1000));
	suffix="-alpha";
fi

#version=1002004
majorVersion=$(($version/1000000));
minorVersion=$(($version%1000000/1000));
buildVersion=$(($version%1000));

echo "$majorVersion.$minorVersion.$buildVersion$suffix";
