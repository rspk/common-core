using System;
using System.Linq;
using RSPK.CommonCore.Extensions;
using Xunit;

// ReSharper disable ParameterOnlyUsedForPreconditionCheck.Local

namespace RSPK.CommonCore.Tests.Extensions
{
	public class UrlHelpersTest
	{
		[Theory]
		[InlineData("ws://wapi.local:5000/ws", "tenantId", 2, "ws://wapi.local:5000/ws?tenantId=2")]
		[InlineData("ws://wapi.local:5000/ws/", "tenantId", 3, "ws://wapi.local:5000/ws/?tenantId=3")]
		[InlineData("ws://wapi.local:5000/ws/?tenantId=2", "tenantId", 3, "ws://wapi.local:5000/ws/?tenantId=3")]
		[InlineData("http://wapi.local:5000", "tenantId", 3, "http://wapi.local:5000/?tenantId=3")]
		public void SetQueryStringTest(string url, string paramName, object paramValue, string expectedUrl)
		{
			InternalSetQueryStringTest(new Uri(url), paramName, paramValue, expectedUrl);
		}

		private void InternalSetQueryStringTest(Uri url, string paramName, object paramValue, string expectedUrl)
		{
			string actualUrl = url.SetQueryString(paramName, paramValue).ToString();
			Assert.Equal(expectedUrl, actualUrl);
		}
	}
}
